class ImageManager
  class << self
    def save_image(imgpath, name)

      mime = %x(/usr/local/bin/exiftool -MIMEType #{imgpath})[34..-1].rstrip
      if mime.nil? || !VALID_MIME.include?(mime)
        return { status: 'failure', message: "#{name} uses an invalid format." }
      end

      hash = Digest::MD5.file(imgpath).hexdigest
      image = Image.find_by_imghash(hash)

      if image.nil?
        image = Image.new
        image.mimetype = mime
        image.imghash = hash
        unless image.save!
          return { status: 'failure', message: "Failed to save #{name}." }
        end

        if Rails.env.production?
          begin
            Aws::S3::Client.new.put_object(bucket: 'veuwer', content_type: image.mimetype, key: "images/#{image.id.to_s(36)}.png", body: File.open(imgpath, 'rb'))
          rescue Aws::S3::Errors::ServiceError => e
            Rails.logger.debug e.to_s
            image.destroy
            return { status: 'failure', message: "Failed to transfer #{name} to storage." }
          end
        else
          unless File.directory?(Rails.root.join('uploads'))
            Dir.mkdir(Rails.root.join('uploads'))
          end
          File.open(Rails.root.join('uploads', "#{image.id.to_s(36)}.png"), 'wb') { |f| f.write(File.open(imgpath, 'rb').read) }
        end
      end

      link = ImageLink.new
      link.image = image
      link.save

      return { status: 'success', message: link.id.to_s(36) }
    end

private

    VALID_MIME = %w(image/png image/jpeg image/gif)
  end
end
