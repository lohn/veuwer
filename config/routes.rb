Rails.application.routes.draw do
  constraints subdomain: 'www' do
    get ':any', to: redirect(subdomain: nil, path: '/%{any}'), any: /.*/
  end

  get 'i/:id.png' => 'main#image_direct'
  get 'i/:id.jpg' => 'main#image_direct'
  get 'i/:id.jpeg' => 'main#image_direct'
  get 'i/:id.gif' => 'main#image_direct'
  get 'i/:id.mp4' => 'main#mp4'
  get 'i/:id.webm' => 'main#webm'
  get 'i/:id.gvid' => 'main#gvid'

  get ':id.png' => 'main#image_direct', constraints: { id: /[a-zA-Z0-9]*/ }
  get ':id.jpg' => 'main#image_direct', constraints: { id: /[a-zA-Z0-9]*/ }
  get ':id.jpeg' => 'main#image_direct', constraints: { id: /[a-zA-Z0-9]*/ }
  get ':id.gif' => 'main#image_direct', constraints: { id: /[a-zA-Z0-9]*/ }

  get 'i/:id' => 'main#image'

  get 'transparency' => 'main#transparency'
  match 'upload' => 'main#upload', via: [:post]

  root 'main#index'
end
